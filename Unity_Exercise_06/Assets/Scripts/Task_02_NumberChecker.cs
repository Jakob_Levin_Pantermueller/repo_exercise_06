﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class Task_02_NumberChecker : MonoBehaviour
    {
        public int[] Array;
        public int numberToSkip = 1;
        public int numberToBreakout = 2;

        void FirstVoid()
        {
            for (int i = 0; i < Array.Length; i++)
            {
                Debug.Log(Array[i]);

                if(i == Array.Length)
                {
                    Debug.Log("The loop is finish and the Method was succesful.");
                }
            }
        }

        void SecondVoid(int argument)
        {
            for(int i = 0; i < Array.Length; i++)
            {
                if(Array[i] == argument)
                {
                    Debug.Log("Skip");
                }
                else
                {
                    Debug.Log(Array[i]);
                }
                if(i == 4)
                {
                    Debug.Log("Succesful");
                }
            }
        }

        void ThirdVoid(int firstArgument, int secondArgument)
        {
            for(int i = 0; i < Array.Length; i++)
            {
                if(Array[i] == firstArgument)
                {
                    Debug.Log("Skip");
                }
                else
                {
                    Debug.Log(Array[i]);
                }
                if (Array[i] == secondArgument)
                {
                    Debug.Log("Finish");
                }
                if(i == 4)
                {
                    Debug.Log("Over");
                }
            }
        }
    }
}