﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class Task_01_Spawner : MonoBehaviour
    {
        public int spawnCounter;

        private void FirstSpawnLoop()
        {
            for (int i = 0; i < spawnCounter; i++)
            {
                GameObject newGo = new GameObject();

                if (spawnCounter >= 1)
                {
                    Debug.Log("Halte dich bereit!");
                }
            }
        }

        private void SecondSpawnLoop(int enemyNumber)
        {
            while (enemyNumber < spawnCounter)
            {
                GameObject newGo = new GameObject();
                spawnCounter++;

                if (spawnCounter >= 1)
                {
                    Debug.Log("Halte dich bereit!");
                }
            }
        }

        private void ThirdSpawnLoop(int enemyNumber)
        {
            do
            {
                GameObject newGo = new GameObject();
                spawnCounter++;

                if (spawnCounter >= enemyNumber)
                {
                    Debug.Log("Halte dich bereit!");
                }
            }
            while (enemyNumber < spawnCounter);
        }
    }
}