﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class Task_03 : MonoBehaviour
    {
        private int[] SecondArray;
        private bool[] ThirdArray = new bool[2];
        private string[] FourthArray = new string[3];

        private void Init()
        {
            SecondArray = new int[3];
            SecondArray[0] = 1;
            SecondArray[1] = 2;
            SecondArray[2] = 3;

            for(int i = 0; i < SecondArray.Length; i++)
            {
                Debug.Log(SecondArray[i]);
            }

            for(int i = 0; i < ThirdArray.Length; i++)
            {
                ThirdArray[i] = true;
                Debug.Log(ThirdArray[i]);
            }

            for(int i = 0; i < FourthArray.Length; i++)
            {
                FourthArray[0] = "Erster Text";
                FourthArray[1] = "Zweiter Text";
                FourthArray[2] = "Dritter Text";
            }
        }

        private void Start()
        {
            Init();
        }
    }
}